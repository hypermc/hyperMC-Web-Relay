
# hyperMC-Web-Relay

This relays Hyper Relay connection hashes over wildcard DNS and wildcard virtualhost reverse proxy to allow for automatic URLs via connection topics.

Set up instructions:

This will require a server listening to a connection topic using our version of hyper-relay:


https://git.ssh.surf/hypermc/hyper-relay

`git clone git@git.ssh.surf:hypermc/hyper-relay.git`

`cd hyper-relay`

`npm i`

Now listen to a TCP port over the Hyper Network:
```
root@server:/hyper-relay# node server.js 80 hellothere
w7zpwusw6ljrgbffxtklak23rkhaxwhs2kskozteme6ofefevlia
```

The result above shows the connection hash for your hyper port. 

This will be the hash used as your subdomain later in this process.

NOTE: The "hellothere" above is the connection "topic" this is used to generate the hash and it can be any string or key phrase you wish.


# The Reverse proxy

We will proxy all of the traffic to their specific peers using a reverse proxy method, in this example, we will utilize httpd/apache below you will find the standard configuration that will work for our purposes.

```
<VirtualHost *:443>
        SSLProxyEngine On   
        ServerAlias *.your-tld.here
        ServerAdmin webmaster@your-tld.here
        DocumentRoot /var/www/html

        ErrorLog /var/log/error.log
        CustomLog /var/log/access.log combined

        SSLCertificateFile /path/to/fullchain.pem
        SSLCertificateKeyFile /path/to/privkey.pem
	    
        # Reverse Proxy for subdomains
        <Proxy *>
            Require all granted
        </Proxy>

        ProxyPreserveHost On
        RequestHeader set X-Forwarded-Proto https
        ProxyPass "/"  "http://localhost:8081/"
        ProxyPassReverse "/"  "http://localhost:8081/"
    </VirtualHost>

```

NOTE 1: This requires `mod_proxy` and `mod_ssl` to work properly. 

NOTE 2: This requires the full use of the wildcard *.your-told.here you will not be able to use any subdomain within this scope unless it runs via a hyper-relay.

We highly reccomend using SSL only for this reverse connection, however, you may configure your proxy pass as desired. 

# Wildcard DNS Entry

In order to serve your subdomain hashes without the need to generate an A record create a wildcard subdomain record. 

Example:
`*.your-tld.here.    IN    A    192.0.2.1`

This system does work well with cloudflare proxy, feel free to proxy your traffic!

# Running the Web Relay Server

`git clone git@git.ssh.surf:hypermc/hyperMC-Web-Relay.git`

`cd hyperMC-Web-Relay`

`npm i`

There are two servers provided within this repo.

One will work without the use of workers, and it can work well for static content sites.

To deploy this server, run `node non-worker-server.js`

The relay will begin listening on port 8081, this port may be changed in the script.
```
[root@server hyperMC-Web-Relay]# node fullyworking_proxy.js 
Creating HyperDHT server...
HyperDHT server created and ready.
HTTP server listening on port 8081
```

To deploy a multi worker server, run `node worker-server.js` 

The server will start workers based off your CPU count and then start those workers listening for connections:

```
[root@server hyper-web-relay]# node worker-server.js
Master 703855 is running
Total Workers 4
Worker 703885 started
Worker 703878 started
Worker 703904 started
Worker 703929 started
Worker 703878 listening on port 8081
Worker 703965 listening on port 8081
Worker 704044 listening on port 8081
Worker 703885 listening on port 8081
```

You now can make a request from a hyper-relay connection hash:

```
~ ❯ curl -IL https://w7zpwusw6ljrgbffxtklak23rkhaxwhs2kskozteme6ofefevlia.your-tld.here/
HTTP/2 200
date: Fri, 29 Mar 2024 06:39:38 GMT
content-type: text/html; charset=UTF-8
last-modified: Tue, 19 Mar 2024 05:07:41 GMT
vary: Accept-Encoding
cf-cache-status: DYNAMIC
report-to: {"endpoints":[{"url":"https:\/\/a.nel.cloudflare.com\/report\/v4?s=mGhuDrFp%2B2lMm9iQ45CshdOmN7Sbf%2Fs1dgWKD3PtcISbcBi6uB4YZIG2qTTmfRpL1C1f3vqF01IFERjjpDspEuQ98dRfgQpelyODFjd99aeUY5uGQcPxQoAusjb%2BBxwRWkI6ZQE0EPKy4m%2BeGbY0%2FGLVndHc%2F52vEvvAIIhzR%2FqMIweFqgyAwGAaELc7sUIDayE%3D"}],"group":"cf-nel","max_age":604800}
nel: {"success_fraction":0,"report_to":"cf-nel","max_age":604800}
server: cloudflare
cf-ray: 86bddd2349cf8bbb-ATL
alt-svc: h3=":443"; ma=86400
```