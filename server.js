const net = require('net');
const b32 = require("hi-base32");
const DHT = require("@hyperswarm/dht");
const node = new DHT({});
const pump = require("pump");
const { pipeline } = require('stream');
const server = net.createServer(clientSocket => {
    clientSocket.on('end', () => {
        console.log('Client disconnected');
    });

    clientSocket.on('error', err => {
        console.error('Client socket error:', err);
    });

    clientSocket.once("data",(firstData) => {
        const firstLine = firstData.toString()
        if (firstLine !== '') {
            console.log(firstLine)
            const [hostname] = firstLine.split("\n").filter(a=>a.toLowerCase().startsWith('host:'));
            const split = hostname.split(":")[1].trim().split('.');
            console.log(split);
            const publicKey = Buffer.from(b32.decode.asBytes(split[0].toUpperCase()), 'hex');
            console.log(publicKey.toString('hex'))
            const socket = node.connect(publicKey);
            socket.on('client error', console.error);
            pump(socket,clientSocket,socket)
            socket.write(firstLine);
        }
    });
});
server.listen(8081, () => {
    console.log('Proxy server listening');
});
